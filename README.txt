This is a Lucida Console with the `0` being crossed, used FontForge free utility to edit it, this change is for personal private use only. 

I prefer the 'Lucida Console' as a programming font, it's main benefit being able to fit more lines on a screen compared to other ones I've tried.